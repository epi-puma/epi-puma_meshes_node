const { gql } = require('apollo-server');

const schema = gql`
  extend type Query {
  	echo_meshes: String!,
    get_mesh(grid_res: String!): [Cell!],
    get_cell_by_coordinates(mesh: String!, longitude: Float, latitude: Float): [Cell!]
  }
`;

module.exports = schema;
