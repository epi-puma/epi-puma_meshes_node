const cell_type = require('./cell_type');
const polygon_scalar = require('./polygon_scalar');

module.exports = [
	cell_type,
	polygon_scalar,  
];
