const { gql } = require('apollo-server');

module.exports = gql`
  type Cell {
    cve: String!,
    geometry: GeoJSONPolygon!,
    simplified_geom: GeoJSONPolygon!
  }
`;
