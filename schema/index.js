const { mergeTypes } = require('merge-graphql-schemas');
const { gql } = require('apollo-server');
const queries = require('./query.js');
const types = require('./types');
const interfaces = require('./interfaces');

let schema = interfaces;
schema = schema.concat(types);
schema.push(queries);
const merged_types = mergeTypes(schema);
module.exports = gql`${merged_types}`;