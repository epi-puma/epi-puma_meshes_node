const { ApolloServer } = require('apollo-server');
const { buildFederatedSchema } = require('@apollo/federation');

const debug = require('debug');
const schema = require('./schema');
const resolvers = require('./resolvers');
const PSQLConnector = require('./connectors/psql');
const config = require('./config');

const log = debug('EPI-PUMA-meshes:log');
const error_log = debug('EPI-PUMA-meshes:error');

const context = {
  conn: {
	  psql: new PSQLConnector(config.db).getConnection()
  },
  logger: {
    log,
    error_log,
  }
};

const federated_schema = buildFederatedSchema([{typeDefs: schema, resolvers: resolvers}]);

const server = new ApolloServer({ schema: federated_schema, context: context});

server.listen({ port: config.server.port }).then(({ url }) => {
  console.log(`Server is ready at ${url}`);
});
