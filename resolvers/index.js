const lodash = require('lodash');
const { Polygon } = require('graphql-geojson-scalar-types');
const { query_provider } = require('../data_source');

const resolvers = {
  Query: {
  	  echo: (parent, args) => 'Hello',
      get_mesh: async (parent, {grid_res}, context) => {
        return query_provider.get_mesh.get_mesh(grid_res, context);
      },
      get_cell_by_coordinates: async (parent, {mesh, longitude, latitude}, context) => {
      	return query_provider.get_cell_by_coordinates.get_cell_by_coordinates(mesh, longitude, latitude, context);
      },
    },
  GeoJSONPolygon: Polygon,
};

module.exports = resolvers;
