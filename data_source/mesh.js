async function get_mesh(grid_res, context) {
  
  console.log('Llamada a BD all_covariable');
  
  const conn = await context.conn.psql;

  const query = `
    SELECT 
    	cve, 
    	ST_AsGeoJSON(ST_Transform(geometry, 4326)) as geometry, 
    	ST_AsGeoJSON(ST_Transform(simplified_geom, 4326)) as simplified_geom
    FROM mesh_${grid_res}
    ORDER BY cve
    `;

  const results = await conn.any(query);

  var N = results.length
  
  for(var i = 0; i < N; i++){
  	results[i]['geometry'] = JSON.parse(results[i]['geometry'])
  	results[i]['simplified_geom'] = JSON.parse(results[i]['simplified_geom'])
  }

  return results;
}

exports.get_mesh = get_mesh;