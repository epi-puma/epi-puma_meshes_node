const get_mesh = require('./mesh');
const get_cell_by_coordinates = require('./get_cell_by_coordinates');

exports.query_provider = {
  get_mesh: get_mesh,
  get_cell_by_coordinates: get_cell_by_coordinates,
};