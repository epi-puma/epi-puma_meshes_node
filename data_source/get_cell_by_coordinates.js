async function get_cell_by_coordinates(mesh, longitude, latitude, context){

  const conn = await context.conn.psql;

  const query = `
    SELECT 
      cve, 
      ST_AsGeoJSON(ST_Transform(geometry, 4326)) as geometry, 
      ST_AsGeoJSON(ST_Transform(simplified_geom, 4326)) as simplified_geom
    FROM mesh_${mesh}
    WHERE ST_Intersects(ST_SetSRID(geometry, 4326), ST_SetSRID(ST_Point(${longitude}, ${latitude}), 4326))
    `;

  const results = await conn.any(query);

  var N = results.length
  
  for(var i = 0; i < N; i++){
    results[i]['geometry'] = JSON.parse(results[i]['geometry'])
    results[i]['simplified_geom'] = JSON.parse(results[i]['simplified_geom'])
  }

  return results;

}

exports.get_cell_by_coordinates = get_cell_by_coordinates;