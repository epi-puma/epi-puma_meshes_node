const pgp = require('pg-promise')();

class PSQLConnector {
  constructor(config) {
    this.config = config;
  }

  async getConnection() {
	const pool = pgp(this.config);
    return pool;
  }
}

module.exports = PSQLConnector;